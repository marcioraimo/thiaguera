Vamos lá!

Tecnologias usadas:
-NodeJS como servidor;
(https://nodejs.org/)

-Express como framework para a gestão de rotas, respostas, requests; basicamente, um HTTPServer
(https://expressjs.com/)

-PM2 como gerenciador de processos (ou seja, não vamos subir o servidor 'normalmente', usando o comando: 'node', vamos usar os comandos do PM2,
pra ele mesmo gerenciar o servidor; se der algum erro e o servidor cair, ou precisar atualizar o servidor devido a uma mudança no código,
o PM2 faz isso pra gente; e com ele, é possível subir vários 'servidores' de uma vez, isso facilita a gente, pra poder usar microserviços,
que é uma estrutura, onde cada parte do servidor é independente, assim, se o serviço de 'autenticação' cair, o resto da aplicação não cai);
(http://pm2.keymetrics.io/)

-Redbird como proxy reverso (isso ajuda a gente a poder usar microserviços);
(https://github.com/OptimalBits/redbird)

-Firebase como SDK para autenticação, banco de dados NOSQL, storage de arquivos, envio de notificações (e uma caralhada de opção que ele da)
(https://firebase.google.com/)
(https://github.com/firebase/firebase-js-sdk)

----------//////////----------//////////----------//////////

Uso:
Primeiramente, é preciso ter o NodeJS e o NPM instalados.
Segundamente, é preciso instalar o PM2 através do comando: 'npm install -g pm2' (sem aspas, o -g instala globalmente, assim, vc pode usar o comando 'pm2' no terminal)
Terceiramente, instalar os pacotes do projeto: npm install (isso faz com que o npm leia o arquivo 'package.json' e instale as dependências)
Quartamente, com tudo instalado, só rodar 'pm2 start index.json', isso vai fazer com que o PM2 inicie os serviços que estão descritos no arquivo.

----------//////////----------//////////----------//////////

Anatomia do index.json
O arquivo é usado pelo PM2 para subir diversos serviços.
Ele é um vetor de objetos, onde cada um é um serviço.

{
    "script": "./index.js",
    "name": "index",
    "watch": "./index.js"
}

-script: é o arquivo que será iniciado 'como um servidor'. Se vc rodar um 'node [e o arquivo que está no script]' vai ter o mesmo resultado
-name: é o nome do serviço
-watch: é o caminho do que ficar 'olhando', por atualizações, modificações e etc, para poder reiniciar o serviço com as modificações e etc

Após subir todos os serviços, o PM2 disponibiliza uma interfacezinha gráfica para monitoramento: 'pm2 monit'.

Para parar os serviços: 'pm2 stop'.
Para matar os serviços: 'pm2 kill'.

----------//////////----------//////////----------//////////

Vc deve estar pensando, 'Blz, cada serviço é como se fosse um servidor, mas então, cada um deve estar rodando em uma porta diferente'
E sim, cada serviço que está dentro do index.json está rodando em uma porta diferente, então, como eu ligo tudo numa grande e única coisa?
Usando o 'redbird', um proxy reverso.
Como assim?
Com o redbird, eu crio um serviço 'principal', onde vc vai fazer as requisições para ele, então, ele saberá para onde enviar a sua requisição.

No arquivo index.js (na pasta raiz) está sendo importado o redbird e apontando para a porta 80, assim, todas as requisições pro seu servidor vão ser nessa porta,
e, de acordo com a 'rota', ele vai saber para qual porta jogar.

----------//////////----------//////////----------//////////

Vc vai ser um MONTE de arquivo 'index.js', por quê?
Não é necessário, mas assim, o Node trabalha com o esquema de 'módulos', e todo caminho principal de um módulo é o arquivo index.js.
Quando vc importa um módulo (através do require()), vc está pegando o arquivo index.js e usando as propriedades dele.
Se eu não usasse o arquivo index.js, eu teria que dar o caminho específico do arquivo do qual estou puxando.

----------//////////----------//////////----------//////////

A pasta 'node_modules' é aonde ficarão todos os pacotes que vc instalar por 'npm', essa pasta geralmente é ignorada por projetos git