// Estou referenciando um arquivo, por mais que eu não esteja colocando a extensão
const modulo1 = require('./modulo1/modulo1');

// Aqui, um módulo
const modulo2 = require('./modulo2');

// const significa constante, mas, porque usar uma constante como importação de um módulo, ou arquivo?
// Pois a constante tem um espaço de memória fixo, já que nunca vai mudar, assim, para coisas estáticas,
// economiza memória

// Tente rodar 'node arquivoPrincipal.js' com o terminal nesta pasta

console.log(modulo1.funcao1);
console.log(modulo1.funcao2);

console.log(modulo2.funcao1);
console.log(modulo2.funcao2);