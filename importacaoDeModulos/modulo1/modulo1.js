/**
 * Esse é um padrão de documentação de JS.
 * 
 * Como JS não tem tipagem, a documentação é importante,
 * principalmente para os editores de texto te darem dicas
 * do que a função precisa e vai te retornar.
 * 
 * Se vc deixar o mouse em cima do nome da função, o VSCode
 * vai te dar as dicas das quais eu estou falando
 * 
 * @returns {string}
 */
function funcao1 () {
    return 'Função 1';
}

/**
 * @returns {string}
 */
function funcao2() {
    return 'Função 2';
}

/**
 * module.exports significa que vc está exportando algo desse módulo.
 * Se vc reparar, eu usei = {}, para representar de que eu estou exportando
 * um monte de coisa de uma vez
 */
module.exports = {
    funcao1,
    funcao2
}