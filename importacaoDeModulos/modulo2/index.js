/**
 * Assim é um outro meio de exportar coisas de um módulo (pessoalmente, eu prefiro o jeito do modulo1)
 * @returns {string}
 */
module.exports.funcao1 = function () {
    return 'Função 1';
}

/**
 * @returns {string}
 */
module.exports.funcao2 = function () {
    return 'Função 2';
}