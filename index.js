// 'require' é a mesma coisa que 'import' do JAVA, ou 'script src' do HTML

// Estou importando o módulo/pacote 'redbird' e assinando ele na porta 80
const redbird = require('redbird')({ port: 80 });

// Aqui, está sendo registrada a rota 'localhost/teste' que vai para a porta 9001
// A porta 80 é omitida, por ser a porta HTTP padrão
redbird.register('localhost/teste', 'localhost:9001');