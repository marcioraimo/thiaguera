// Importação do 'express', um pacote/lib que ajuda MUITO no roteamento e etc
const express = require('express');

// Estou criando uma instância do Express, e chamando de 'app'
const app = express();

// Criação de uma rota no método GET, para o caminho padrão
app.get('/', (request, response) => {
    response.status(200).send('Teste de GET');
});

// Criação de uma rota no método POST, para o caminho padrão
app.post('/', (request, response) => {
    response.status(200).send('Teste de POST');
});

// Criação de uma rota no método PUT, para o caminho padrão
app.put('/', function (request, response) {
    response.status(200).send('Teste de PUT');
});

// Criação de uma rota no método DELETE, para o caminho padrão
app.delete('/', function (request, response) {
    response.status(200).send('Teste de DELETE');
});

// Na criação da rota, o Express precisa que vc fale qual vai ser o 'nome' da rota, e a função que vai ser executada quando a rota for encontrada
// Nessa função, vc precisa receber 2 objetos, Request e Response, que dizem a respeito da requisição e da resposta

// Note que em 2 rotas eu uso function(){} e em outras 2 uso ()=>{}
// A segunda notação é uma notação de ES6, para encurtar a sintaxe de funções anônimas
// Eles fazem EXATAMENTE a mesma coisa, só escreve diferente

// Aqui, estou 'subindo' o servidor, falando pra ele escutar a porta 9001
app.listen(9001);